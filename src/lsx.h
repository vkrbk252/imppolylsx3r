/*
 * lsx.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef LSX_H_
#define LSX_H_

#include <iostream>
#include "common.h"
#include "sbox.h"
#include "matrix.h"

class LSX
{
  unsigned int state_size_;
  unsigned int rounds_;
  SBox<uint8_t> sbox_;
  Matrix<uint8_t> linear_matrix_;
  ByteVector state_;
  Byte2Vector keys_;
  std::vector<Byte2Vector> lookup_tables_;
  std::vector<Byte2Vector> lookup_tables_inv_;
  SBox<uint8_t> invsbox_;
  Matrix<uint8_t> invlinear_matrix_;

  void FormLookupTables();

 public:
  LSX(const unsigned int state_size, const unsigned int rounds,
      const SBox<uint8_t>& sbox, const Matrix<uint8_t>& linear_matrix)
      : state_size_(state_size),
        rounds_(rounds),
        sbox_(sbox),
        linear_matrix_(linear_matrix),
        state_(ByteVector(state_size, 0)),
        keys_(Byte2Vector(rounds + 1, ByteVector(state_size, 0))),
        lookup_tables_(
            std::vector<Byte2Vector>(
                state_size, Byte2Vector(256, ByteVector(state_size, 0)))),
        lookup_tables_inv_(
            std::vector<Byte2Vector>(
                state_size, Byte2Vector(256, ByteVector(state_size, 0))))
  {
    invsbox_ = sbox_.GenerateInverse();
    invlinear_matrix_ = linear_matrix_.GenerateInverse();
    FormLookupTables();
  }

  LSX() = default;

  LSX(const LSX& rhs)
      : state_size_(rhs.state_size_),
        rounds_(rhs.rounds_),
        sbox_(rhs.sbox_),
        linear_matrix_(rhs.linear_matrix_),
        state_(rhs.state_),
        keys_(rhs.keys_),
        lookup_tables_(rhs.lookup_tables_),
        lookup_tables_inv_(rhs.lookup_tables_inv_),
        invsbox_(rhs.invsbox_),
        invlinear_matrix_(rhs.invlinear_matrix_)
  {
  }

  ByteVector& get_state();
  Byte2Vector& get_keys();
  const SBox<uint8_t>& get_sbox();
  SBox<uint8_t>& get_invsbox();
  Matrix<uint8_t>& get_linear_matrix();
  Matrix<uint8_t>& get_invlinear_matrix();

  void AddRoundKey(const ByteVector &key);
  void SubBytes();
  void LinearTransform();
  void InvSubBytes();
  void InvLinearTransform();
  void LSXRound(const ByteVector &key);
  void InvLSXRround(const ByteVector &key);

  void Show();
};

#endif /* LSX_H_ */
