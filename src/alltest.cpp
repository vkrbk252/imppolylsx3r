/*
 * alltest.cpp
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#include <gtest/gtest.h>
#include "common.h"
#include "alltest.h"

#include "difference.h"
#include "lsx.h"
#include "sbox.h"

TEST(Difference, GetSize)
{
  Difference diff(5, 6);
  ASSERT_EQ(5, diff.get_size());
}

TEST(Difference, GetColumn)
{
  Byte2Vector data = {{0x03, 0x04}, {0x05, 0x06}, {0x07, 0x08}};
  ByteVector column = {0x06, 0x04};
  Difference diff(data);
  ASSERT_EQ(diff.get_column(0), column);
}

TEST(Difference, GetAnchor)
{
  Byte2Vector data = {{0x03, 0x04}, {0x05, 0x06}, {0x07, 0x08}};
  ByteVector anchor = {0x03, 0x04};
  Difference diff(data);
  ASSERT_EQ(diff.get_anchor(), anchor);
}

TEST(SBox, Inverse)
{
  SBox<uint8_t> magma = {0x0C, 0x04, 0x6, 0x2, 0xA, 0x5, 0xB, 0x9, 0xE, 0x8,
      0xD, 0x7, 0x0, 0x3, 0xF, 0x1};
  SBox<uint8_t> inverse = magma.GenerateInverse();
  SBox<uint8_t> inv_correct = {12, 15, 3, 13, 1, 5, 2, 11, 9, 7, 4, 6, 0, 10, 8,
      14};
  for (size_t i = 0; i < 16; ++i)
  {
    ASSERT_EQ(inverse[i], inv_correct[i]);
  }
}

TEST(Matrix, Inverse)
{
  Matrix<uint8_t> kuzn_matrix_r({0xcf, 0x6e, 0xa2, 0x76, 0x98, 0x20, 0xc8, 0x33,
                                    0x74, 0xc6, 0x87, 0x10, 0xbf, 0xda, 0x70,
                                    0x0c},
                                0xc3);
  auto inverse = kuzn_matrix_r.GenerateInverse();
  Matrix<uint8_t> kuzn_inv_matrix({170, 91, 236, 204, 152, 49, 58, 165, 223,
                                      169, 167, 67, 146, 193, 175, 83},
                                  0xc3);
  for (size_t i = 0; i < 16; ++i)
  {
    ASSERT_EQ(inverse[i], kuzn_inv_matrix[i]);
  }
}

TEST(LSX, KuznechikOneRound)
{
  ByteVector zeros(0);
  ByteVector xk = {0x99, 0xbb, 0x99, 0xff, 0x99, 0xbb, 0x99, 0xff, 0xff, 0xff,
      0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
  ByteVector sxk = {0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6, 0xb6, 0xb6,
      0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6};
  ByteVector lsxk = {0xe2, 0x97, 0xb6, 0x86, 0xe3, 0x55, 0xb0, 0xa1, 0xcf, 0x4a,
      0x2f, 0x92, 0x49, 0x14, 0x08, 0x30};
  const int kStateSize = 16;
  const int kRounds = 1;
  LSX kuznechik(kStateSize, kRounds, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() =
  { 0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x00,0xff,0xee,0xdd,0xcc,0xbb,0xaa,0x99,0x88};

  kuznechik.AddRoundKey(kuzn_keys[0]);
  ASSERT_EQ(kuznechik.get_state(), xk);

  kuznechik.SubBytes();
  ASSERT_EQ(kuznechik.get_state(), sxk);

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);

}

TEST(LSX, KuznechikFull)
{
  ByteVector zeros(0);
  ByteVector xk = {0x99, 0xbb, 0x99, 0xff, 0x99, 0xbb, 0x99, 0xff, 0xff, 0xff,
      0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
  ByteVector sxk = {0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6, 0xb6, 0xb6,
      0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6};
  ByteVector lsxk = {0xe2, 0x97, 0xb6, 0x86, 0xe3, 0x55, 0xb0, 0xa1, 0xcf, 0x4a,
      0x2f, 0x92, 0x49, 0x14, 0x08, 0x30};
  ByteVector ethalon = {0x7f, 0x67, 0x9d, 0x90, 0xbe, 0xbc, 0x24, 0x30, 0x5a,
      0x46, 0x8d, 0x42, 0xb9, 0xd4, 0xed, 0xcd};
  const int kStateSize = 16;
  const int kRounds = 9;
  LSX kuznechik(kStateSize, kRounds, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() =
  { 0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x00,0xff,0xee,0xdd,0xcc,0xbb,0xaa,0x99,0x88};

  kuznechik.AddRoundKey(kuzn_keys[0]);
  ASSERT_EQ(kuznechik.get_state(), xk);

  kuznechik.SubBytes();
  ASSERT_EQ(kuznechik.get_state(), sxk);

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);

  for (int i = 1; i < kRounds; i++)
  {
    kuznechik.LSXRound(kuzn_keys[i]);
  }
  kuznechik.AddRoundKey(kuzn_keys[kRounds]);
  ASSERT_EQ(kuznechik.get_state(), ethalon);
}

TEST(LSX, KhazadFull)
{
  const int kStateSize = 8;
  const int kRounds = 8;
  ByteVector ethalon = {0x49, 0xA4, 0xCE, 0x32, 0xAC, 0x19, 0x0E, 0x3F};
  LSX khazad(kStateSize, kRounds, khazad_sbox, khazad_matrix);
  //khazad.FormLookupTables();
  Byte2Vector khazad_keys(11, ByteVector(kStateSize, 0));
  Byte2Vector cts(9, ByteVector(kStateSize, 0));

  for (int i = 0; i < 9; i++)
  {
    for (int j = 0; j < 8; j++)
    {
      cts[i][j] = khazad_sbox[8 * i + j];
    }
  }

  ByteVector tmp(khazad_master_key.begin(), khazad_master_key.begin() + 8);
  khazad_keys[0] = tmp;
  ByteVector tmp1(khazad_master_key.end() - 8, khazad_master_key.end());
  khazad_keys[1] = tmp1;

  LSX ctx(kStateSize, kRounds, khazad_sbox, khazad_matrix);

  for (int i = 2; i < 11; i++)
  {
    ctx.get_state() = khazad_keys[i - 1];
    ctx.SubBytes();
    ctx.LinearTransform();
    ctx.AddRoundKey(cts[i - 2]);
    khazad_keys[i] = khazad_keys[i - 2] ^ ctx.get_state();
    Show(khazad_keys[i]);
  }

  for (int i = 0; i < kRounds - 1; i++)
  {
    khazad.LSXRound(khazad_keys[i + 2]);
  }
  khazad.AddRoundKey(khazad_keys[kRounds + 1]);
  khazad.SubBytes();
  khazad.AddRoundKey(khazad_keys[kRounds + 2]);
  ASSERT_EQ(khazad.get_state(), ethalon);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

