/*
 * main.cpp
 *
 *  Created on: Oct 29, 2018
 *      Author: sesve
 */

#include <iostream>
#include <chrono>
#include <string>
#include <fstream>
#include <omp.h>

#include "cxxopts.hpp"
#include "experiment.h"
#include "alltest.h"
#include "common.h"

int main(int argc, char* argv[])
{
  int kPolytopeSize = 6;
  int kStateSize = 4;
  int kDepth = 0;
  uint8_t poly = 0xc3;
  std::string sbox_path, matrix_path;
  int argc_count = 0;
  bool random = false;

  try
  {
    cxxopts::Options options(
        argv[0],
        "Атака на основе многомерных невозможных дифференциалов на 3-раундовые LSX-шифры");

    options.add_options()("p, polytopes", "Число открытых текстов",
                          cxxopts::value<int>()->default_value("6"))(
        "d, depth", "Глубина компромисса время-память",
        cxxopts::value<int>()->default_value("0"))(
        "l, length", "Размер блока в байтах",
        cxxopts::value<int>()->default_value("4"))(
        "s, sbox", "Байтовая подстановка", cxxopts::value<std::string>(),
        "FILE.bin")("m, matrix", "Байтовая матрица линейного преобразования",
                    cxxopts::value<std::string>(), "FILE.bin")(
        "polynomial", "Неприводимый полином",
        cxxopts::value<uint8_t>()->default_value("0xc3"))(
        "r, random",
        "Использование случайной подстановки и матрицы линейного преобразования",
        cxxopts::value<bool>()->default_value("false"))(
        "h, help", "Вывод информационного сообщения");

    auto result = options.parse(argc, argv);

    if (result.count("polytopes"))
    {
      kPolytopeSize = result["polytopes"].as<int>();
      argc_count++;
    }

    if (result.count("length"))
    {
      kStateSize = result["length"].as<int>();
      argc_count++;
    }

    if (result.count("depth"))
    {
      kDepth = result["depth"].as<int>();
      argc_count++;
    }

    if (result.count("sbox"))
    {
      sbox_path = result["sbox"].as<std::string>();
      argc_count++;
    }

    if (result.count("matrix"))
    {
      matrix_path = result["matrix"].as<std::string>();
      argc_count++;
    }

    if (result.count("polynomial"))
    {
      poly = result["polynomial"].as<uint8_t>();
      argc_count++;
    }

    if (result.count("random"))
    {
      random = result["random"].as<bool>();
      argc_count++;
    }

    if (result.count("help") || (argc_count == 0))
    {
      std::cout << options.help({""}) << std::endl;
      exit(0);
    }

  }
  catch (const cxxopts::OptionException& e)
  {
    std::cout << "error parsing options: " << e.what() << std::endl;
    exit(1);
  }

  try
  {
    if (kPolytopeSize < 3)
      throw ExperimentException(
          "Количество открытых текстов должно быть не менее 3!", "main");
    if ((kStateSize < 4) || (kStateSize > 6))
      throw ExperimentException(
          "Длина блока должна быть не менее 4 и не более 6 байт!", "main");
    if ((kDepth < 0) || (kDepth > 1))
      throw ExperimentException(
          "Некорректный компромисс время-память! Сложность равна полному перебору!",
          "main");
    if (poly == 0x00)
      throw ExperimentException(
          "Неприводимый многочлен не может быть равен нулю!", "main");
  }
  catch (ExperimentException &e)
  {
    std::cout << e.what() << std::endl;
    exit(1);
  }

  std::vector<uint8_t> matrix;
  std::vector<uint8_t> sbox;

  SBox<uint8_t> ex_sbox;
  Matrix<uint8_t> ex_matrix;

  if (random == false)
  {
    try
    {
      sbox = GetFromFile<uint8_t>(sbox_path.c_str(), 256);
      matrix = GetFromFile<uint8_t>(matrix_path.c_str(),
                                    kStateSize * kStateSize);
    }
    catch (FileException &e)
    {
      std::cout << e.what() << std::endl;
      exit(1);
    }

    ex_sbox = SBox<uint8_t>(sbox);
    ex_matrix = Matrix<uint8_t>(matrix, poly);
  }
  else
  {
    ex_sbox = GenerateRandomS();
    try
    {
      ex_matrix = GenerateRandomM(kStateSize * kStateSize, poly);
    }
    catch (ExperimentException &e)
    {
      std::cout << e.what() << std::endl;
      exit(1);
    }
  }

  const int kRounds = 3;

  kDepth = ceil((kStateSize - 2) / 2) + 2 - kDepth;

  auto start_time = std::chrono::steady_clock::now();

  LSX cipher(kStateSize, kRounds, ex_sbox, ex_matrix);
  Experiment experiment(kPolytopeSize, kRounds, kStateSize, cipher, kDepth);
  experiment.Run();
  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_s = std::chrono::duration_cast<std::chrono::seconds>(
      end_time - start_time);
  std::cout << std::dec << "Experiment total time: " << elapsed_s.count()
            << " s" << std::endl;
  return 0;
}
