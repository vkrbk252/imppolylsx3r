#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 11:03:41 2018

@author: sesve
"""

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from scipy import stats
import numpy as np

def solutions(block_size, diff_size, cipher_name, file):
    num_exp=256
    num_sbox=np.linspace(1,block_size,block_size)
    num_of_solutions=[]
    for i in range(1,block_size+1):
        if (2**(8*(block_size+1-i*(diff_size-1)))) < 1:
            num_of_solutions.append(1)
        else:
            num_of_solutions.append(2**(8*(block_size+1-i*(diff_size-1))))
    print(num_of_solutions)
    
    plt.scatter(x=num_sbox, y=np.log2(num_of_solutions), marker='D',c='b')
    
    lI, s1, s2, s3, s4, s5, time = np.genfromtxt(file, delimiter=',', unpack=True)
    plt.scatter([1]*num_exp, y=np.log2(s1), marker='.',c='r') 
    plt.scatter([2]*num_exp, y=np.log2(s2), marker='.',c='r')
    plt.scatter([3]*num_exp, y=np.log2(s3), marker='.',c='r')
    plt.scatter([4]*num_exp, y=np.log2(s4), marker='.',c='r')
    plt.scatter([5]*num_exp, y=np.log2(s5), marker='.',c='r')

    #plt.scatter(x=1, y=np.log2(np.mean(s1)), marker='.',c='g') 
    #plt.scatter(x=2, y=np.log2(np.mean(s2)), marker='.',c='g') 
    #plt.scatter(x=3, y=np.log2(np.mean(s3)), marker='.',c='g') 
    #plt.scatter(x=4, y=np.log2(np.mean(s4)), marker='.',c='g') 
    
    plt.plot(num_sbox,np.log2(num_of_solutions), label = "Теретическое", ls=':') 
  
    #plt.plot(num_sbox,[np.log2(s1),np.log2(s2),np.log2(s3),np.log2(s4)], label = "Theory", ls=':')
    plt.xticks(range(1,block_size+1))
    plt.xlabel('Номер S-бокса')
    plt.ylabel('Число решений $(log_2)$')
    plt.grid()
    plt.title(cipher_name + r' $n=$'+str(block_size)+r'$, d=$'+str(diff_size))
    theory_line = mlines.Line2D([], [], color='blue', marker='D', linestyle=':',label='Теоретическое')
    practise_line = mlines.Line2D([], [], color='red', marker='.', linestyle='',label='Экспериментальное')
    plt.legend(handles=[theory_line, practise_line])
    plt.savefig(cipher_name +'_sol_n='+str(block_size)+'_d='+str(diff_size)+'.png', dpi=800)
    plt.show()
    
    
def solution_var(block_size, diff_size, cipher_name, file):
    num_exp=256
    num_exper=np.arange(1,num_exp+1,1);
    sol_th=2**(8*(block_size+2-diff_size))
    th=[sol_th]*num_exp
    plt.plot(num_exper,np.log2(th), label = "Теоретическое", ls=':')
    plt.xlim(0,num_exp)
    lI, x, s2, s3, s4, s5, time = np.genfromtxt(file, delimiter=',', unpack=True)
    #x, y = np.genfromtxt('sol.txt', delimiter=',', unpack=True)
    plt.scatter(num_exper, np.log2(x), marker='.',c='g')  #наносим все экспериментальные
    mean=[np.mean(x)]*num_exp
    print('Mean solutions=',np.mean(x),' Sigma=',np.std(x))
    plt.plot(num_exper,np.log2(mean), label = "Экспериментальное", ls=':', c='r')    
    plt.xlabel('Номер эксперимента')
    plt.ylabel('Число решений ($log_2$)')
    plt.title('Число решений (' + cipher_name +', n='+str(block_size)+', d='+str(diff_size)+')')
    plt.grid()
    plt.legend()
    plt.savefig(cipher_name +'_solvar_n='+str(block_size)+'_d='+str(diff_size)+'.png', dpi=800)
    plt.show()
   
    
def solution_hist_dist(block_size, diff_size, cipher_name, file):
    lI, x, s2, s3, s4, s5, time = np.genfromtxt(file, delimiter=',', unpack=True)
    #x, y = np.genfromtxt('sol.txt', delimiter=',', unpack=True)
    from collections import Counter
    c=Counter(x)
    k_list=[]
    for key in c.keys():
        k_list.append(key)    
    plt.hist(x, len(k_list), density=True,color='lightcoral')  
    xt = plt.xticks()[0]  
    xmin, xmax = min(xt), max(xt)  
    lnspc = np.linspace(xmin, xmax, len(x))
    m, s = stats.norm.fit(x) # get mean and standard deviation  
    pdf_g = stats.norm.pdf(lnspc, m, s) # now get theoretical values in our interval  
    plt.plot(lnspc, pdf_g, label="Norm", ls=':', c='b') # plot it  
   
    plt.xlabel('Число решений')
    plt.ylabel('Вероятность')
    plt.title('Распределение числа решений (' + cipher_name +', n='+str(block_size)+', d='+str(diff_size)+')')
    plt.grid()
    plt.legend()
    plt.savefig(cipher_name +'_solhistdist_n='+str(block_size)+'_d='+str(diff_size)+'.png', dpi=800)
    plt.show()
    
    
def solutions_series(block_size, diff_size, cipher_name, file):
    solutions(block_size, diff_size, cipher_name, file)
    plt.clf()
    plt.cla()
    plt.close()
    solution_var(block_size, diff_size, cipher_name, file)
    plt.clf()
    plt.cla()
    plt.close()
    solution_hist_dist(block_size, diff_size, cipher_name, file)
    
     
def time_practise(block_size, cipher_name, folder):
    diff_size=np.arange(np.floor(block_size/2)+2, block_size+4, 1)
    time_res=[]
    for i in range(block_size//2 + 2, block_size+4):
        time = np.genfromtxt(folder+str(block_size)+
        str(i)+'.txt', delimiter=',', unpack=True, usecols=(block_size+1))
        time_res.append(np.mean(time))
        print(np.mean(time))
    plt.plot(diff_size,time_res,':.', label = "Время")
        
    plt.xticks(range(block_size//2 + 2, block_size+4, 1))
    plt.xlabel('Размер многомерной разности')
    plt.ylabel('Время, с')
    plt.grid()
    plt.title('Время атаки (' + cipher_name +', n='+str(block_size)+')')
    plt.legend()
    plt.savefig(cipher_name +'_time_n='+str(block_size)+'.png', dpi=800)
    
def sols_diff(block_size, cipher_name, folder):
    diff_size=np.arange(np.floor(block_size/2)+2, block_size+4, 1)
    sol_res=[]
    for i in range(block_size//2 + 2, block_size+4):
        sol = np.genfromtxt(folder+str(block_size)+str(i)+'.txt', delimiter=',', unpack=True, usecols=(1))
        plt.scatter(x=[i]*256, y=np.log2(sol), marker='o',c='r')
        sol_res.append(np.log2(np.mean(sol)))
    plt.plot(diff_size,sol_res,':.', label = "Теоретическое")
    plt.xticks(range(block_size//2 + 2, block_size+4, 1))
    plt.xlabel('Размер многомерной разности')
    plt.ylabel('Число решений $(log_2)$')
    plt.grid()
    plt.title(cipher_name +', n='+str(block_size))
    theory_line = mlines.Line2D([], [], color='blue', marker='.', linestyle=':',label='Теоретическое')
    practise_line = mlines.Line2D([], [], color='red', marker='.', linestyle='',label='Экспериментальное')
    plt.legend(handles=[theory_line, practise_line])
    plt.savefig(cipher_name +'_sol1_n='+str(block_size)+'.png', dpi=800)
    
#CONSTANT GRAPHICS
    
def dmin():
    block_size=np.arange(4,17,1)
    d_min=np.floor(block_size/2)+2
    plt.scatter(x=block_size, y=d_min, marker='.',c='r')
    plt.plot(block_size,d_min, label = "Difference size", ls=':')
    plt.xlabel('Block size')
    plt.ylabel('Difference size')
    plt.grid()
    plt.title('Minimal difference size')
    plt.legend()
    plt.show()
    #plt.savefig('dmin.png', dpi=800)
    
def time_memory():
    block_size=np.arange(4,17,1)
    diff_size=np.floor(block_size/2)+2
    time=2**(8*(block_size-np.floor(block_size/2)+1))
    memory=diff_size*2**(8*(np.floor(block_size/2)+1))
    plt.scatter(x=block_size, y=np.log2(time), marker='.',c='r')
    plt.plot(block_size,np.log2(time), label = "Time", ls=':')
    plt.scatter(x=block_size, y=np.log2(memory), marker='.',c='g')
    plt.plot(block_size,np.log2(memory), label = "Memory", ls=':')
    plt.xlabel('Block size')
    plt.ylabel('Complexity ($log_2$)')
    plt.grid()
    plt.title('Time and memory complexity')
    plt.legend()
    plt.show()
    #plt.savefig('time_memory.png', dpi=800)

def time_memory_comp():
    ax = plt.gca()
    for block_size in range(6,17,2):
        diff_size=1#np.floor(block_size/2)+2
        t=np.arange(0,np.floor(block_size/2)-1,1)
        time=2**(8*(block_size-np.floor(block_size/2)+1+t))
        memory=diff_size*2**(8*(np.floor(block_size/2)+1-t))
        col = next(ax._get_lines.prop_cycler)['color']
        plt.plot(t,np.log2(time),ls=':',color=col,label='n='+str(block_size))        
        plt.plot(t,np.log2(memory),ls='--',color=col)
        plt.scatter(x=t, y=np.log2(time), marker='.',c=col)
        plt.scatter(x=t, y=np.log2(memory), marker='v',c=col)
    plt.xlabel('t')
    plt.ylabel('Complexity ($log_2$)')
    plt.grid()
    plt.title('Time-memory trade-off')
    n_legend=plt.legend()
    mem_line = mlines.Line2D([], [], color='black', ls='--', marker='v', label='Memory')
    time_line = mlines.Line2D([], [], color='black', ls=':', marker='.', label='Time')
    plt.legend(handles=[mem_line, time_line])
    plt.gca().add_artist(n_legend)
    plt.show()
    #plt.savefig('time_memory_trd.png', dpi=800)
    
def const_gr():
    dmin()
    plt.clf()
    plt.cla()
    plt.close()
    time_memory()
    plt.clf()
    plt.cla()
    plt.close()
    time_memory_comp()

    

    

    
