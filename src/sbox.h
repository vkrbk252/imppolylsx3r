/*
 * sbox.h
 *
 *  Created on: Oct 16, 2018
 *      Author: sesve
 */

#ifndef SBOX_H_
#define SBOX_H_

#include <initializer_list>
//#include "common.h"

template<typename Field>
class SBox
{
  std::vector<Field> sbox_;

 public:
  SBox()
  {
    sbox_ = std::vector<Field>(2 << (8 * sizeof(Field)), 0);
  }

  SBox(std::vector<Field> sbox)
      : sbox_(sbox)
  {
  }
  //For test only
  SBox(std::initializer_list<Field> list)
      : sbox_(list)
  {
  }

  Field& operator[](size_t n)
  {
    return sbox_[n];
  }

  SBox<Field> GenerateInverse()
  {
    SBox<Field> inverse;
    for (auto& x : sbox_)
    {
      inverse[sbox_[x]] = x;
    }
    return inverse;
  }
};

#endif /* SBOX_H_ */
