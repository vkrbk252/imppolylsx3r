/*
 * experiment.cpp
 *
 *  Created on: Sep 28, 2018
 *      Author: sesve
 */

#include "experiment.h"
#include "matrix.h"
#include "sbox.h"
#include "difference.h"
#include "lsx.h"
#include "common.h"
#include <omp.h>
#include <utility>
#include <algorithm>

Experiment::Experiment(int polytope_size, int rounds, int block_size,
                       LSX cipher, int depth_rec)
{
  polytope_size_ = polytope_size;
  rounds_ = rounds;
  block_size_ = block_size;
  cipher_ = LSX(cipher);
  depth_rec_ = depth_rec;
  for (int i = 0; i < rounds_ + 1; i++)
  {
    final_diff_trail_.emplace_back(Difference(polytope_size_ - 1, block_size_));
  }
}

Experiment::~Experiment()
{

}

Byte2Vector Experiment::GeneratePlaintexts()
{
  Byte2Vector texts(polytope_size_, ByteVector(block_size_, 0));
  ChoosenPlaintexts(texts);
  std::cout << "Plaintexts generated... " << std::endl;
  return texts;
}

Byte2Vector Experiment::GenerateKeys()
{
  Byte2Vector keys(rounds_ + 1, ByteVector(block_size_, 0));
  for (int i = 0; i < rounds_ + 1; i++)
  {
    RandomVector(keys[i]);
  }
  std::cout << "Keys generated... " << std::endl;
  std::cout << "----------------------------------------------" << std::endl;
  std::cout << "Last key : ";
  Show(keys[rounds_]);
  std::cout << "----------------------------------------------" << std::endl;

  return keys;
}

Byte2Vector Experiment::GenerateCiphertexts(Byte2Vector& ptexts,
                                            Byte2Vector& keys)
{
  Byte2Vector texts(polytope_size_, ByteVector(block_size_, 0));
  for (int i = 0; i < polytope_size_; i++)
  {
    cipher_.get_state() = ptexts[i];
    for (int i = 0; i < rounds_; i++)
    {
      cipher_.LSXRound(keys[i]);
    }
    cipher_.AddRoundKey(keys[rounds_]);
    texts[i] = cipher_.get_state();
  }
  std::cout << "Ciphertexts generated... " << std::endl;
  std::cout << std::endl;
  return texts;
}

void Experiment::GetPossibleDiffS(const ByteVector& diff_column,
                                  SBox<uint8_t>& sbox, DiffContainer& ddt)
{
  for (int x = 0; x < 256; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
    }
    ddt[tmp] = diff_column;
  }
}

Byte2Vector Experiment::GetPossibleDiffS(const ByteVector& diff_column,
                                         SBox<uint8_t>& sbox)
{
  Byte2Vector ddt;
  for (int x = 0; x < 256; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
    }
    ddt.push_back(tmp);
  }
  return ddt;
}

DiffSet Experiment::GetSetPossibleDiffS(const ByteVector& diff_column,
                                        SBox<uint8_t>& sbox)
{
  DiffSet ddt;
  for (int x = 0; x < 256; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
    }
    ddt.insert(tmp);
  }
  return ddt;
}

void Experiment::GetPossibleDiffSL(const ByteVector &diff_column,
                                   SBox<uint8_t>& sbox, DiffContainer& ddt)
{
  for (int x = 0; x < 256; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
      tmp[i] = cipher_.get_linear_matrix().mult_tables_[0][tmp[i]];
    }
    ddt[tmp] = diff_column;
  }
}

void Experiment::FirstSboxSLSOut(std::vector<DiffContainer>& diff_tree)
{
  ByteVector diff_column = final_diff_trail_[0].get_column(0);
  auto sbox = cipher_.get_sbox();

  std::cout << "-------------Precomputation phase-------------" << std::endl;
  GetPossibleDiffSL(diff_column, sbox, diff_tree[0]);

  std::cout << "Generated 1st round [2**8] possible differences..."
            << std::endl;

  for (auto it = diff_tree[0].begin(); it != diff_tree[0].end(); it++)
  {
    GetPossibleDiffS(it->first, sbox, diff_tree[1]);
  }
  if (diff_tree[1].size() != 65536)
  {
    std::cout << "----------------------------------------------" << std::endl;
    std::cout
        << "WARNING! More than 1 parent in alpha_sls differences! Wrong result is possible!"
        << std::endl;
    std::cout << "----------------------------------------------" << std::endl;
  }
  std::cout << "Generated 2nd round [2**16] possible differences..."
            << std::endl;
}

Byte2Vector Experiment::FormListFromTree(const DiffContainer& diff_tree)
{
  Byte2Vector SLSList;
  for (const auto& it : diff_tree)
  {
    SLSList.push_back(it.first);
  }
  return SLSList;
}

void Experiment::InvLinearDiff(Difference& beta, LSX& cipher)
{
  for (int i = 0; i < beta.get_size(); i++)
  {
    cipher.get_state() = beta.get_row(i);
    cipher.InvLinearTransform();
    beta.get_row(i) = cipher.get_state();
  }
}

Byte2Vector Experiment::GetPossibleDiffSMulC(const ByteVector& diff_column,
                                             SBox<uint8_t>& sbox,
                                             const uint8_t& c)
{
  Byte2Vector ddt;
  for (int x = 0; x < 256; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
      tmp[i] = cipher_.get_invlinear_matrix().mult_tables_[c][tmp[i]];
    }
    ddt.push_back(tmp);
  }
  return ddt;
}

std::vector<Byte2Vector> Experiment::InvSboxAllListsMuled(
    const int& matrix_column)
{
  std::vector<Byte2Vector> pd_cont;
  Difference beta = final_diff_trail_[rounds_];
  InvLinearDiff(beta, cipher_);
  auto sbox = cipher_.get_invsbox();
  auto invmatrix = cipher_.get_invlinear_matrix();
  for (int i = 0; i < block_size_; i++)
  {
    auto matrix_elem = block_size_ * i + matrix_column;
    pd_cont.push_back(
        GetPossibleDiffSMulC(beta.get_column(i), sbox, matrix_elem));
  }
  std::cout << "Generated list of beta`s possible differences muled with "
            << std::dec << matrix_column + 1 << " row..." << std::endl;
  return pd_cont;
}

void Experiment::RecursiveGenListsThr(
    MulListsCont& lI_thread, const std::vector<Byte2Vector>& possible_diff)
{
  bool parallel_enable = (block_size_ > 5);
#pragma omp parallel for
  for (size_t i = 0; i < possible_diff[0].size(); i++)
  {
    std::vector<int> nums;
    nums.push_back(i);
    RecursiveGenLists(1, possible_diff[0][i], lI_thread, possible_diff, nums);
  }
}

void Experiment::RecursiveGenLists(
    int depth, const ByteVector& elem, MulListsCont& lI,
    const std::vector<Byte2Vector>& possible_diff, std::vector<int> nums)
{
  if (depth == depth_rec_)
  {
#pragma omp critical
    {
      auto insert_result = lI.insert({elem, nums});
      if (insert_result.second == false)
      {
        auto it = insert_result.first;
        it->second.insert(it->second.end(), nums.begin(), nums.end());
      }
    }
    return;
  }
  for (size_t i = 0; i < possible_diff[depth].size(); i++)
  {
    nums.push_back(i);
    RecursiveGenLists(depth + 1, possible_diff[depth][i] ^ elem, lI,
                      possible_diff, nums);
    nums.pop_back();
  }
}

void Experiment::CommonDifferencesThr(
    const MulListsCont& lI, const std::vector<Byte2Vector>& possible_diff,
    std::vector<std::vector<std::vector<int>>>& solutions)
{
#pragma omp parallel for
  for (int i = 0; i < possible_diff[depth_rec_].size(); i++)
  {
    auto cur_thread = omp_get_thread_num();
    std::vector<int> nums{i};
    //nums.push_back(i);
    CommonDifferences(1, possible_diff[depth_rec_][i], possible_diff, nums, lI,
                      solutions[cur_thread]);
  }
}

void Experiment::CommonDifferences(
    int depth, const ByteVector& elem,
    const std::vector<Byte2Vector>& possible_diff, std::vector<int> nums,
    const MulListsCont& lI, std::vector<std::vector<int> >& solutions)
{
  if (depth == (block_size_ + 1 - depth_rec_))
  {
    //#pragma omp critical
    //{
    auto it = lI.find(elem);
    if (it != lI.end())
    {
      size_t num_of_sums = it->second.size() / depth_rec_;
      for (size_t i = 0; i < num_of_sums; i++)
      {
        std::vector<int> current_solution;
        current_solution.insert(current_solution.end(),
                                it->second.begin() + i * depth_rec_,
                                it->second.begin() + (i + 1) * depth_rec_);
        current_solution.insert(current_solution.end(), nums.begin(),
                                nums.end());
        solutions.push_back(current_solution);
      }
    }
    //}
    return;
  }
  for (size_t i = 0; i < possible_diff[depth + depth_rec_].size(); i++)
  {
    nums.push_back(i);
    CommonDifferences(depth + 1, possible_diff[depth + depth_rec_][i] ^ elem,
                      possible_diff, nums, lI, solutions);
    nums.pop_back();
  }
}

Byte2Vector Experiment::BuildDiffTrail(
    const std::vector<DiffContainer>& ddt_tree,
    const ByteVector& common_difference)
{
  Byte2Vector diff_trail;
  diff_trail.push_back(common_difference);
  ByteVector trail_part = common_difference;
  for (size_t i = 0; i < ddt_tree.size(); i++)
  {
    auto it_to_parent = ddt_tree[ddt_tree.size() - 1 - i].find(trail_part);
    trail_part = it_to_parent->second;
    diff_trail.push_back(trail_part);
  }
  reverse(begin(diff_trail), end(diff_trail));
  return diff_trail;
}

ByteVector Experiment::GetColumnAfterS(const ByteVector &diff_column,
                                       SBox<uint8_t>& sbox,
                                       Matrix<uint8_t>& matrix,
                                       const ByteVector &out_diff_column)
{
  for (int x = 0; x < 256; x++)
  {
    ByteVector afterS(diff_column.size(), 0);
    ByteVector afterL(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      afterS[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
      afterL[i] = matrix.mult_tables_[0][afterS[i]];
    }
    if (afterL == out_diff_column)
    {
      return afterS;
    }
  }
  return ByteVector(diff_column.size(), 0);
}

Difference Experiment::FormAlphaS(const ByteVector& diff_column)
{
  Difference alpha_s(polytope_size_ - 1, block_size_);
  alpha_s.set_column(diff_column, 0);
  return alpha_s;
}

void Experiment::LinearDiff(Difference& alpha, LSX& cipher)
{
  for (int i = 0; i < alpha.get_size(); i++)
  {
    cipher.get_state() = alpha.get_row(i);
    cipher.LinearTransform();
    alpha.get_row(i) = cipher.get_state();
  }
}

std::vector<DiffSet> Experiment::SLSOutLists(Difference& alpha_ls)
{
  std::vector<DiffSet> lists;
  auto sbox = cipher_.get_sbox();
  for (int i = 0; i < block_size_; i++)
  {
    lists.push_back(GetSetPossibleDiffS(alpha_ls.get_column(i), sbox));
  }
  return lists;
}

std::vector<Byte2Vector> Experiment::InvSboxAllLists()
{
  std::vector<Byte2Vector> pd_cont;
  Difference beta = final_diff_trail_[rounds_];
  InvLinearDiff(beta, cipher_);
  auto& sbox = cipher_.get_invsbox();
  for (int i = 0; i < block_size_; i++)
  {
    pd_cont.push_back(GetPossibleDiffS(beta.get_column(i), sbox));
  }
  std::cout << "Generated list of beta`s possible differences..." << std::endl;
  return pd_cont;
}

Difference Experiment::FormDifferenceFromListsElems(
    const std::vector<int>& nums, const std::vector<Byte2Vector>& lists)
{
  Difference beta_inv_s(polytope_size_ - 1, block_size_);
  for (int i = 0; i < block_size_; i++)
  {
    beta_inv_s.set_column(lists[i][nums[i]], i);
  }
  return beta_inv_s;
}

void Experiment::TextsPrecomputation()
{
  Byte2Vector plaintexts = GeneratePlaintexts();
  final_diff_trail_[0] = Difference(plaintexts);
  cipher_.get_keys() = GenerateKeys();
  Byte2Vector ciphertexts = GenerateCiphertexts(plaintexts, cipher_.get_keys());
  final_diff_trail_[rounds_] = Difference(ciphertexts);
}

void Experiment::ShowParams()
{
  std::cout << "Polytope size : " << polytope_size_ << " texts" << std::endl;
  std::cout << "Rounds : " << rounds_ << std::endl;
  std::cout << "Block size : " << block_size_ << " bytes" << std::endl;
  std::cout << "Depth : " << depth_rec_ << " lists in memory" << std::endl;
  std::cout << std::endl;
  std::cout
      << "Time complexity : "
      << "[2**"
      << 8
          * (block_size_ - (floor(block_size_ / 2)) + 1
              + (ceil((block_size_ - 2) / 2) + 2 - depth_rec_))
      << "] memory access operations" << std::endl;
  std::cout
      << "Memory complexity : "
      << (polytope_size_ - 1)
      << " * "
      << "[2**"
      << 8
          * (floor(block_size_ / 2) + 1
              - (ceil((block_size_ - 2) / 2) + 2 - depth_rec_))
      << "] bytes" << std::endl;
  std::cout << std::endl;
}

ByteVector Experiment::BrutePreKey(
    const std::vector<Difference>& final_solutions)
{
  ByteVector key;
  Difference beta(final_diff_trail_[rounds_]);
  InvLinearDiff(beta, cipher_);
  auto& sbox_inv = cipher_.get_invsbox();

  for (size_t i = 0; i < final_solutions.size(); i++)
  {
    Difference candidat = final_solutions[i];
    for (int k = 0; k < block_size_; k++)
    {
      ByteVector diff_column = candidat.get_column(k);
      ByteVector beta_column = beta.get_column(k);
      for (int x = 0; x < 256; x++)
      {
        ByteVector tmp(polytope_size_ - 1, 0);
        bool isWrong = false;
        for (int i = 0; i < polytope_size_ - 1; i++)
        {
          tmp[i] = sbox_inv[x ^ beta_column[i]] ^ sbox_inv[x];
          if (tmp[i] != diff_column[i])
          {
            isWrong = true;
            break;
          }
        }
        if (isWrong == false)
        {
          key.push_back(x);
          break;
        }
      }
    }
  }
  return key;
}

void Experiment::KeyRecovery(ByteVector& key)
{
  ByteVector anchor = final_diff_trail_[rounds_].get_anchor();
  cipher_.get_state() = anchor;
  cipher_.InvLinearTransform();
  anchor = key ^ cipher_.get_state();
  cipher_.get_state() = anchor;
  cipher_.LinearTransform();
  key = cipher_.get_state();
}

void Experiment::Run()
{
  ShowParams();
  TextsPrecomputation();
  std::vector<DiffContainer> alpha_sls(rounds_ - 1);
  FirstSboxSLSOut(alpha_sls);
  std::vector<Byte2Vector> lists = InvSboxAllListsMuled(0);
  lists.push_back(FormListFromTree(alpha_sls.back()));

  MulListsCont lI;
  std::vector<MulListsCont> lI_thread(omp_get_max_threads());

  ByteVector zeroes(polytope_size_ - 1, 0);
  std::vector<int> nums;

  unsigned long long size_mul = 1;
  for (int i = 0; i < depth_rec_; i++)
    size_mul *= lists[i].size();
  unsigned long long size_iter = 1;
  for (int i = depth_rec_; i < block_size_ + 1; i++)
    size_iter *= lists[i].size();

  std::cout << "Theoretical size of multiplication list : " << size_mul
            << " [2**" << std::log2(size_mul) << "]" << std::endl;

  RecursiveGenListsThr(lI, lists);

  std::cout << "Generated multiplication list..." << std::endl;
  std::cout << "Practical size of multiplication list: : " << std::dec
            << lI.size() << " [2**" << std::log2(lI.size()) << "]" << std::endl;
  std::cout << std::endl;

  std::vector<std::vector<std::vector<int>>> solutions_thr(
      omp_get_max_threads());
  std::vector<std::vector<int> > solutions;
  std::vector<int> nums2;

  std::cout << "-----------------Search phase-----------------" << std::endl;

  CommonDifferencesThr(lI, lists, solutions_thr);

  size_t all_solutions = 0;
  for (size_t i = 0; i < solutions_thr.size(); i++)
  {
    all_solutions += solutions_thr[i].size();
  }

  std::cout << "Theoretical number of 1st sbox solutuions : "
            << (size_mul * size_iter) / pow(2, 8 * (polytope_size_ - 1));
  std::cout
      << " [2**"
      << std::log2((size_mul * size_iter) / pow(2, 8 * (polytope_size_ - 1)))
      << "]" << std::endl;
  std::cout << "Practical number of 1st sbox solutions : " << std::dec
            << all_solutions;
  std::cout << " [2**" << std::log2(all_solutions) << "]" << std::endl;
  std::cout << std::endl;

  lI.clear();

  std::cout << "------------------Check phase-----------------" << std::endl;

  lists = InvSboxAllLists();
  lists.push_back(FormListFromTree(alpha_sls.back()));

  /*массив решений на каждом шаге для каждого потока*/
  std::vector<std::vector<size_t>> num_sol_thread(
      omp_get_max_threads(), std::vector<size_t>(block_size_, 0));

  /*массив решений на каждом шаге общий*/
  std::vector<size_t> num_solutions_on_step(block_size_, 0);
  num_solutions_on_step[0] = all_solutions;

  /*массив решений-разностей для каждого потока*/
  std::vector<std::vector<Difference>> final_sol_thread(omp_get_max_threads());

  /*массив решений-разностей общий*/
  std::vector<Difference> final_solutions;

  /*массив шифраторов для каждого потока*/
  std::vector<LSX> cipher_thread(omp_get_max_threads(), cipher_);

  /*массив списков для каждого потока*/
  std::vector<std::vector<Byte2Vector>> lists_thread(omp_get_max_threads(),
                                                     lists);

  /*массив деревьев для каждого потока*/
  std::vector<std::vector<DiffContainer>> alpha_sls_thread(
      omp_get_max_threads(), alpha_sls);

#pragma omp parallel for
  for (size_t j = 0; j < solutions_thr.size(); j++)
  {
    auto cur_thread = omp_get_thread_num();
    for (size_t i = 0; i < solutions_thr[j].size(); i++)
    {
      bool is_connected = true;

      int num_in_SLS = solutions_thr[j][i].back();
      ByteVector diff = lists_thread[cur_thread].back()[num_in_SLS];  //cs
      Byte2Vector diff_trail = BuildDiffTrail(alpha_sls_thread[cur_thread],
                                              diff);
      auto sbox = cipher_thread[cur_thread].get_sbox();
      auto matrix = cipher_thread[cur_thread].get_linear_matrix();

      ByteVector input_diff = diff_trail[0];
      ByteVector out_diff = diff_trail[1];

      ByteVector alpha_s = GetColumnAfterS(input_diff, sbox, matrix, out_diff);
      Difference alpha_ls = FormAlphaS(alpha_s);

      LinearDiff(alpha_ls, cipher_thread[cur_thread]);

      std::vector<DiffSet> lists_sls = SLSOutLists(alpha_ls);
      Difference beta_inv_s(polytope_size_ - 1, block_size_);

      beta_inv_s = FormDifferenceFromListsElems(solutions_thr[j][i],
                                                lists_thread[cur_thread]);
      InvLinearDiff(beta_inv_s, cipher_thread[cur_thread]);

      for (int j = 1; j < block_size_; j++)
      {
        auto it = lists_sls[j].find(beta_inv_s.get_column(j));
        if (it != lists_sls[j].end())
        {
          num_sol_thread[cur_thread][j]++;
        }
        else
        {
          is_connected = false;
          break;
        }
      }
      if (is_connected == true)
      {
        LinearDiff(beta_inv_s, cipher_thread[cur_thread]);
        final_sol_thread[cur_thread].push_back(beta_inv_s);
      }
    }
  }

  for (size_t thr = 0; thr < num_sol_thread.size(); thr++)
  {
    for (const auto& sol_thr : final_sol_thread[thr])
    {
      final_solutions.push_back(sol_thr);
    }
    for (unsigned int sb_num = 1; sb_num < block_size_; sb_num++)
    {
      num_solutions_on_step[sb_num] += num_sol_thread[thr][sb_num];
    }
  }

  for (size_t i = 0; i < num_solutions_on_step.size(); i++)
  {
    std::cout << std::dec << "Solutions on " << i + 1 << " step : "
              << num_solutions_on_step[i] << " [2**"
              << std::log2(num_solutions_on_step[i]) << "]" << std::endl;
  }

  if (num_solutions_on_step[1] == 0)
  {
    std::cout << "Linear" << std::endl;
    cipher_.get_linear_matrix().Show(block_size_);
    std::cout << "===================================" << std::endl;
    cipher_.get_invlinear_matrix().Show(block_size_);
  }
  std::cout << std::endl;
  std::cout << "--------------Key recovery phase--------------" << std::endl;

  ByteVector key = BrutePreKey(final_solutions);
  KeyRecovery(key);

  std::cout << "----------------------------------------------" << std::endl;
  std::cout << "Last key : ";
  Show(key);
  std::cout << "----------------------------------------------" << std::endl;
  std::cout << std::endl;
}

