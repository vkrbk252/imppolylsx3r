/*
 * common.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <vector>
#include <cstdint>

#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>
#include <fstream>

using ByteVector = std::vector<uint8_t>;
using Byte2Vector = std::vector< std::vector<uint8_t> >;

template<typename Container>
struct ContainerHash
{
  std::size_t operator()(Container const& c) const
  {
    return boost::hash_range(c.begin(), c.end());
  }
};

using DiffContainer = std::unordered_map <ByteVector, ByteVector, ContainerHash <ByteVector> >;
using DiffSet = std::unordered_set <ByteVector, ContainerHash <ByteVector > >;
using MulListsCont = std::unordered_map <ByteVector, std::vector<int>, ContainerHash <ByteVector> >;

void Show(const Byte2Vector &a);

void Show(const ByteVector &a);

const ByteVector operator^(const ByteVector& left, const ByteVector& right);

class FileException
{
  std::string error_;
  std::string procedure_;

 public:
  FileException(std::string err, std::string proc)
      : error_(err),
        procedure_(proc)
  {
  }
  std::string what()
  {
    return "Функция: " + procedure_ + "\nОшибка: " + error_;
  }
};

class ExperimentException
{
  std::string error_;
  std::string procedure_;

 public:
  ExperimentException(std::string err, std::string proc)
      : error_(err),
        procedure_(proc)
  {
  }
  std::string what()
  {
    return "Функция: " + procedure_ + "\nОшибка: " + error_;
  }
};

template<typename Field>
std::vector<Field> GetFromFile(const char* filename_in, int size)
{
  std::ifstream input_file(filename_in, std::ifstream::binary);
  if (input_file)
  {
    std::vector<Field> from_file(size, 0);
    char* buffer = new char[size];
    input_file.seekg(0, input_file.beg);
    input_file.read(buffer, size);
    if (input_file)
    {
      for (int i = 0; i < size; i++)
      {
        from_file[i] = buffer[i];
      }
      input_file.close();
      delete[] buffer;
      return from_file;
    }
    else
    {
      input_file.close();
      throw FileException("Ошибка чтения файла! Некорректный размер!",
                          "GetFromFile " + std::string(filename_in));
    }
  }
  else
  {
    input_file.close();
    throw FileException("Ошибка открытия файла!",
                        "GetFromFile " + std::string(filename_in));
  }
}

#endif /* COMMON_H_ */
