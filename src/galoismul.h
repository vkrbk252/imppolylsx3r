/*
 * galoismul.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef GALOISMUL_H_
#define GALOISMUL_H_

#include <cstdint>
#include "common.h"

uint8_t gf_multiply(uint8_t a, uint8_t b, uint8_t field_poly)
{
  uint8_t acc = 0x00;
  uint8_t msb;

  for (uint8_t i = 0; i < 8; ++i)
  {
    if (b & 0x01)
    {
      acc ^= a;
    }
    msb = a & 0x80;
    a <<= 1;
    if (msb)
    {
      a ^= field_poly;
    }
    b >>= 1;
  }
  return acc;
}

#endif /* GALOISMUL_H_ */
