/*
 * experiment.h
 *
 *  Created on: Sep 28, 2018
 *      Author: sesve
 */

#ifndef EXPERIMENT_H_
#define EXPERIMENT_H_

#include "difference.h"
#include "lsx.h"
#include "sbox.h"
#include "generation.h"
#include "common.h"

class Experiment
{
  int polytope_size_;
  int rounds_;
  int block_size_;
  std::vector<Difference> final_diff_trail_;
  LSX cipher_;
  int depth_rec_;

  Byte2Vector GeneratePlaintexts();
  Byte2Vector GenerateKeys();
  Byte2Vector GenerateCiphertexts(Byte2Vector& ptexts, Byte2Vector& keys);

  void GetPossibleDiffS(const ByteVector& diff_column, SBox<uint8_t>& sbox,
                        DiffContainer& ddt);
  Byte2Vector GetPossibleDiffS(const ByteVector& diff_column,
                               SBox<uint8_t>& sbox);
  DiffSet GetSetPossibleDiffS(const ByteVector& diff_column,
                              SBox<uint8_t>& sbox);
  void GetPossibleDiffSL(const ByteVector& diff_column, SBox<uint8_t>& sbox,
                         DiffContainer& ddt);
  void FirstSboxSLSOut(std::vector<DiffContainer>& diff_tree);
  Byte2Vector FormListFromTree(const DiffContainer& diff_tree);
  void InvLinearDiff(Difference& beta, LSX& cipher);
  Byte2Vector GetPossibleDiffSMulC(const ByteVector& diff_column,
                                   SBox<uint8_t>& sbox, const uint8_t& c);
  std::vector<Byte2Vector> InvSboxAllListsMuled(const int& matrix_column);
  void RecursiveGenLists(int depth, const ByteVector& elem, MulListsCont& lI,
                         const std::vector<Byte2Vector>& possible_diff,
                         std::vector<int> nums);
  void RecursiveGenListsThr(
      /*std::vector<MulListsCont>& lI_thread*/MulListsCont& lI_thread,
      const std::vector<Byte2Vector>& possible_diff);
  void CommonDifferences(int depth, const ByteVector& elem,
                         const std::vector<Byte2Vector>& possible_diff,
                         std::vector<int> nums, const MulListsCont& lI,
                         std::vector<std::vector<int>>& solutions);
  void CommonDifferencesThr(
      const MulListsCont& lI, const std::vector<Byte2Vector>& possible_diff,
      std::vector<std::vector<std::vector<int>>>& solutions);
  Byte2Vector BuildDiffTrail(const std::vector<DiffContainer>& ddt_tree,
                             const ByteVector& common_difference);
  ByteVector GetColumnAfterS(const ByteVector &diff_column, SBox<uint8_t>& sbox,
                             Matrix<uint8_t>& matrix,
                             const ByteVector &out_diff_column);
  Difference FormAlphaS(const ByteVector& diff_column);
  void LinearDiff(Difference& alpha, LSX& cipher);
  std::vector<DiffSet> SLSOutLists(Difference& alpha_ls);
  std::vector<Byte2Vector> InvSboxAllLists();
  Difference FormDifferenceFromListsElems(
      const std::vector<int>& nums, const std::vector<Byte2Vector>& lists);
  ByteVector BrutePreKey(const std::vector<Difference>& final_solutions);
  void KeyRecovery(ByteVector& key);

  void TextsPrecomputation();
  void ShowParams();

 public:
  Experiment(int polytope_size, int rounds, int block_size, LSX cipher,
             int depth_rec);
  ~Experiment();
  void Run();

};

#endif /* EXPERIMENT_H_ */
